public class BKing extends Piece{

    private int[][] validCoordRef;
    public BKing(int xCoord, int yCoord, Piece[][] boardStatus){
        super(xCoord, yCoord, "B", boardStatus);
        int count = 0;
        validCoordRef = new int[8][2];
        for(int i = 0; i < 3; ++i){
            for(int j = 0; j < 3; ++j){
                if(i == 1 && j == 1){
                    continue;
                }
                validCoordRef[count][0] = i - 1;
                validCoordRef[count][1] = j - 1;
                ++count;
            }
        }
        bking = this;
    }

    /*public boolean isValidMove(int xMove, int yMove){
        int xDiff = xMove - xCoord;
        int yDiff = yMove - yCoord;
        for(int i = 0; i < 9; ++i) {
            if (validCoords[i][0] == xDiff && validCoords[i][1] == yDiff) {
                return true;
            }
        }
        return false;
    }*/

    @Override
    public void calcValidMoves(){
        possibleMoves = 0;
        resetValidMoves();
        Piece pieceAtDest;
        int xAdd;
        int yAdd;
        for(int i = 0; i < 8; ++i){
            xAdd = validCoordRef[i][0];
            yAdd = validCoordRef[i][1];
            if(!indexInRange(xCoord + xAdd, yCoord + yAdd)){
                continue;
            }
            pieceAtDest = boardStatus[yCoord + yAdd][xCoord + xAdd];
            if(pieceAtDest == null || (pieceAtDest != null && !pieceAtDest.team.equals(team))){
                if(!moveLeaveKingCheck(xCoord + xAdd, yCoord + yAdd)){
                    validMoves[yCoord + yAdd][xCoord + xAdd] = 1;
                    ++possibleMoves;
                }
            }
        }
        if(canLongCastle()){
            validMoves[7][2] = 1;
            ++possibleMoves;
        }
        if(canShortCastle()){
            validMoves[7][6] = 1;
            ++possibleMoves;
        }
    }

    public boolean isUnderCheck(){
        Piece objAtCheck;
        int tempXCoord = xCoord;
        int tempYCoord = yCoord;

        ++tempYCoord;
        ++tempXCoord;
        for(; indexInRange(tempXCoord, tempYCoord); ++tempXCoord, ++tempYCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof WBishop || objAtCheck instanceof WQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }
        tempXCoord = xCoord;
        tempYCoord = yCoord;

        ++tempYCoord;
        --tempXCoord;
        for(; indexInRange(tempXCoord, tempYCoord); --tempXCoord, ++tempYCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof WBishop || objAtCheck instanceof WQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }
        tempXCoord = xCoord;
        tempYCoord = yCoord;

        --tempYCoord;
        --tempXCoord;
        if(indexInRange(tempYCoord, tempXCoord) && boardStatus[tempYCoord][tempXCoord] instanceof WPawn) {
            return true;
        }
        for(; indexInRange(tempXCoord, tempYCoord); --tempXCoord, --tempYCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof WBishop || objAtCheck instanceof WQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;

        --tempYCoord;
        ++tempXCoord;
        if(indexInRange(tempYCoord, tempXCoord) && boardStatus[tempYCoord][tempXCoord] instanceof WPawn) {
            return true;
        }
        for(; indexInRange(tempXCoord, tempYCoord); ++tempXCoord, --tempYCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof WBishop || objAtCheck instanceof WQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }
        tempXCoord = xCoord;
        tempYCoord = yCoord;

        //ROOK CHECK

        ++tempXCoord;
        for(; indexInRange(tempXCoord, tempYCoord); ++tempXCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof WRook || objAtCheck instanceof WQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }
        tempXCoord = xCoord;
        tempYCoord = yCoord;

        --tempXCoord;
        for(; indexInRange(tempXCoord, tempYCoord); --tempXCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof WRook || objAtCheck instanceof WQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }
        tempXCoord = xCoord;
        tempYCoord = yCoord;

        ++tempYCoord;
        for(; indexInRange(tempXCoord, tempYCoord); ++tempYCoord){
            objAtCheck = boardStatus[tempYCoord][tempXCoord];
            if(objAtCheck instanceof WRook || objAtCheck instanceof WQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }
        tempXCoord = xCoord;
        tempYCoord = yCoord;

        --tempYCoord;
        for(; indexInRange(tempXCoord, tempYCoord); --tempYCoord){
                objAtCheck = boardStatus[tempYCoord][tempXCoord];
                if(objAtCheck instanceof WRook || objAtCheck instanceof WQueen){
                return true;
            }
            if(objAtCheck != null){
                break;
            }
        }

        //KNIGHT PART
        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord++;
        tempYCoord += 2;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof WKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord += 2;
        tempYCoord++;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof WKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord += 2;
        tempYCoord--;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof WKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord++;
        tempYCoord -= 2;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof WKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord--;
        tempYCoord -= 2;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof WKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord -= 2;
        tempYCoord--;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof WKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord -= 2;
        tempYCoord++;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof WKnight){
            return true;
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        tempXCoord--;
        tempYCoord += 2;
        if(indexInRange(tempXCoord, tempYCoord) && boardStatus[tempYCoord][tempXCoord] instanceof WKnight){
            return true;
        }
        return false;
    }


    public boolean canShortCastle(){
        if(boardStatus[7][4] instanceof BKing && !hasMoved && boardStatus[7][5] == null && boardStatus[7][6] == null &&
                boardStatus[7][7] instanceof BRook && !boardStatus[7][7].hasMoved){
            if(!isUnderCheck() && !moveLeaveKingCheck(5, 7) && !moveLeaveKingCheck(6, 7)){
                return true;
            }
        }
        return false;
    }

    public boolean canLongCastle(){
        if(boardStatus[7][4] instanceof BKing && !hasMoved && boardStatus[7][3] == null && boardStatus[7][2] == null && boardStatus[7][1] == null &&
                boardStatus[7][0] instanceof BRook && !boardStatus[7][0].hasMoved){
            if(!isUnderCheck() && !moveLeaveKingCheck(3, 7) && !moveLeaveKingCheck(2, 7)){
                return true;
            }
        }
        return false;
    }

    public String toString(){
        return "BKi";
    }
}
