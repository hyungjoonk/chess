import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import java.io.PrintStream;

public class ClientSide {
    private BufferedReader keyBr, serverBr;
    private PrintStream ps;
    private Socket server;
    //private int clientAssociate;

    public ClientSide() {
        try {
            connect();
            keyBr = new BufferedReader(new InputStreamReader(System.in));
            serverBr = new BufferedReader(new InputStreamReader(server.getInputStream()));
            ps = new PrintStream(server.getOutputStream());

            Thread thread = new Thread(() -> {
                try {
                    String s;
                    while (!(s = serverBr.readLine()).equals("exit")) {
                        receivedFromServer(s);
                    }
                } catch (IOException e) {
                    System.out.println("HERE 1");
                }
            });

        } catch (IOException e) {
            System.out.println("IOEXCEPTION");
        }
    }

    public void sendToServer(String s) {

    }

    public void receivedFromServer(String s) {
        String[] commandArr = s.split("/", -1);

        switch (commandArr[0]) {
            /*case ("CA"):
                clientAssociate = Integer.valueOf(commandArr[1]);
                break;*/

            default:
                break;
        }
    }

    public void connect() {
        try {
            server = new Socket("localhost", 49001);
        } catch (IOException e) {
            System.out.println("ERROR CONNECT");
        }
    }
}
