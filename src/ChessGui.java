import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.awt.event.MouseAdapter;
import java.awt.event.*;
import javax.swing.BorderFactory;
import javax.swing.plaf.basic.BasicButtonUI;

public class ChessGui {//TODO: PROMOTION implement
    private ClientSide cs;
    private JFrame jframe;
    private JPanel panel1;
    private Board board;
    private int pixelGap;

    private int[][][] boardCoord;
    private int[][] validMovesOfPiece;
    private JButton[][] buttonStatus;
    private Piece[][] boardStatus;
    private Color buttonColor;
    private Piece piecePicked;
    private final int moveIndicatorSize;
    private ArrayList<JPanel> toRemoveValidMovesPics;
    private JPanel[][] validMovesPics;
    private String recentCommand;
    private final int pieceImgSize;
    private JLabel[][] pieceImageList;
    private JLabel whiteTimerLabel;
    private JLabel blackTimerLabel;
    private JLabel whiteUserLabel;
    private JLabel blackUserLabel;
    private StringReader sr;
    private Color green;
    private Color beige;
    private double whiteTimeLeft;
    private double blackTimeLeft;
    private double whiteInitialGracePeriod;
    private double timeIncrement;
    private String blackUserName;
    private String whiteUserName;
    private Timer blackTimer;
    private Timer whiteTimer;
    private JLabel invisibleLabel;
    private JButton singleplayer;
    private JButton multiplayer;
    private JButton invisibleButton;
    private long timeInMillisWhenChange;

    private final int pieceImageConstantMod;
    private final int modeSelectButtonWidth = 150;
    private final int modeSelectButtonHeight = 40;

    private ClientSide clientSide;

    public ChessGui(Board board, ClientSide cs) {
        this.cs = cs;
        setBothTime(30, 0);//placeholder, add option to change time
        whiteUserName = "FoxGambit";
        blackUserName = "TempEnemy";


        this.board = board;
        green = new Color(83, 163, 54);
        beige = new Color(245, 245, 220);
        buttonColor = green;
        jframe = new JFrame("FoxGambit's Chess");
        jframe.setSize(1200, 800);
        pieceImageList = new JLabel[8][8];
        sr = new StringReader();
        pieceImageConstantMod = 4;



        validMovesPics = new JPanel[8][8];
        toRemoveValidMovesPics = new ArrayList<>();
        moveIndicatorSize = 20;
        pieceImgSize = 50;

        panel1 = new JPanel();

        jframe.setVisible(true);
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        boardCoord = new int[8][8][2];

        buttonStatus = new JButton[8][8];

        pixelGap = 60;//Pixel gap between two buttons
        calcBoardCoord(jframe.getSize().width / 2, jframe.getSize().height / 2 - jframe.getSize().height / 10, pixelGap);


        startSelectScreen();



        /*panel1.setLayout(null);
        jframe.add(panel1);
        addButton();
        panel1.setVisible(true);
        addBasicInterfaceComponents();

        validMovesOfPiece = new int[8][8];
        boardStatus = board.getBoardStatus();
        piecePicked = null;
        try {
            board.start();
        } catch (BlackKingNotFoundException e) {

        } catch (WhiteKingNotFoundException e) {

        }
        addInitialValidMoves();
        initialPicSetup();

        //panel1.revalidate();
        //panel1.repaint();
        //jframe.revalidate();
        //jframe.repaint();

        jframe.add(invisibleLabel);
        jframe.revalidate();
        jframe.repaint();
        recentCommand = "";*/
        //jframe.add(invisibleLabel);
        //jframe.revalidate();
        //jframe.repaint();
        recentCommand = "";
    }

    public JPanel getMainPanel() {
        return panel1;
    }

    public JFrame getFrame() {
        return jframe;
    }

    public ClientSide getClientSide(){
        return cs;
    }

    public void setBothTime(double timeInMinute, int timeIncrement){
        this.timeIncrement = timeIncrement;
        blackTimeLeft = timeInMinute * 60;
        whiteTimeLeft = timeInMinute * 60;
        if(timeInMinute >= 30){
            whiteInitialGracePeriod = 60;
        }
        else{
            whiteInitialGracePeriod = 30;
        }
    }

    public void calcBoardCoord(int xPixel, int yPixel, int pixelSize) {
        for (int i = 0; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                boardCoord[i][j][0] = xPixel - (4 - j) * pixelSize;
                boardCoord[i][j][1] = yPixel + (4 - i) * pixelSize;
            }
        }
    }

    public void addButton() {
        for (int i = 0; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                JButton temp = new JButton();
                buttonStatus[i][j] = temp;
                temp.setPreferredSize(new Dimension(pixelGap, pixelGap));
                panel1.add(temp);
                temp.setBounds(boardCoord[i][j][0], boardCoord[i][j][1], pixelGap, pixelGap);
                temp.setBackground(buttonColor);
                temp.setBorderPainted(true);
                //temp.setPreferredSize(new Dimension(50, 50));
                temp.setRolloverEnabled(false);
                temp.setUI(new BasicButtonUI(){//remove button pressed animation
                    @Override
                    protected void paintButtonPressed(Graphics g, AbstractButton b){

                    }

                });
                //temp.setFocusable(false);
                //jframe.setComponentZOrder(temp, 0);

                //set listener
                int finalI = i;
                int finalJ = j;
                temp.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {//THROW IT FOR NOW
                        showTimerOnTop();
                        if(pieceImageList[finalI][finalJ] != null) {//PROBABLY DONT NEED
                            //panel1.setComponentZOrder(pieceImageList[finalI][finalJ], 0);
                        }
                        jframe.revalidate();
                        jframe.repaint();
                        if (piecePicked == null) {
                            piecePicked = boardStatus[finalI][finalJ];
                            if (piecePicked == null || !piecePicked.getTeam().equals(board.getCurrTurn())) {
                                return;
                            }
                            //piecePicked.printValidMoves();
                            validMovesOfPiece = piecePicked.getValidMoves();
                            displayValidMovesPics();
                            showTimerOnTop();
                        } else {
                            if (boardStatus[finalI][finalJ] != null &&
                                    boardStatus[finalI][finalJ].getTeam().equals(board.getCurrTurn())) {
                                piecePicked = boardStatus[finalI][finalJ];
                                validMovesOfPiece = piecePicked.getValidMoves();
                                removeValidMovesPics();
                                displayValidMovesPics();
                                showTimerOnTop();
                                return;
                            }
                            if (validMovesOfPiece[finalI][finalJ] == 0) {
                                piecePicked = null;
                                removeValidMovesPics();
                                return;
                            }
                            String moveCommand = "";
                            moveCommand += String.valueOf((char) ('A' + piecePicked.getXCoord()));
                            moveCommand += (piecePicked.getYCoord() + 1 + " ");

                            moveCommand += String.valueOf((char) ('A' + finalJ));
                            moveCommand += String.valueOf(finalI + 1);

                            recentCommand = moveCommand;
                            try {
                                board.movePiece(moveCommand);
                            } catch (IncorrectPromotionPiece ipp) {
                                recentCommand = "";
                            }

                            timeInMillisWhenChange = System.currentTimeMillis();
                            switchTimeAfterTurn();

                            resetValidMoves();
                            piecePicked = null;
                            removeValidMovesPics();
                            updateBoardStatus(recentCommand);
                            if(board.getWWin()){
                                System.out.println("WHITE WIN");
                            }
                            else if(board.getBWin()){
                                System.out.println("BLACK WIN");
                            }
                        }
                        refresh();
                    }
                });
                temp.addMouseListener(new MouseAdapter(){//prevent piece from disappearing
                    @Override
                    public void mouseReleased(MouseEvent e){
                        if(!temp.contains(e.getPoint()) && pieceImageList[finalI][finalJ] != null){
                            panel1.setComponentZOrder(pieceImageList[finalI][finalJ], 0);
                            //System.out.println("HERE");
                            jframe.revalidate();
                            jframe.repaint();
                        }
                    }

                    /*@Override
                    public void mousePressed(MouseEvent e){
                        if(pieceImageList[finalI][finalJ] != null){
                            panel1.setComponentZOrder(pieceImageList[finalI][finalJ], 0);

                            jframe.revalidate();
                            jframe.repaint();
                        }
                    }*/
                });

                if (buttonColor.equals(green)) {
                    buttonColor = beige;
                } else {
                    buttonColor = green;
                }
            }
            if (buttonColor.equals(green)) {
                buttonColor = beige;
            } else {
                buttonColor = green;
            }
        }
    }

    public void clear() {
        jframe.removeAll();
    }

    public void refresh() {
        //refresh valid moves of given piece
        //refresh timer and etc
    }

    public void removeValidMovesPics() {
        for (JPanel pics : toRemoveValidMovesPics) {
            pics.setVisible(false);
        }
        toRemoveValidMovesPics.clear();

        panel1.revalidate();
        panel1.repaint();

        jframe.revalidate();
        jframe.repaint();

    }

    /*public void removeValidMovesPics(){
        for(JPanel pics : toRemoveValidMovesPics){
            pics.setVisible(false);
           //jframe.setComponentZOrder(pics, 1);
        }
        toRemoveValidMovesPics.clear();
        panel1.revalidate();
        panel1.repaint();

        jframe.revalidate();
        jframe.repaint();
    }*/

    /*public void displayValidMovesPics() {
        for (int i = 0; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                int finalI = i;
                int finalJ = j;
                if (validMovesOfPiece[i][j] == 0) {
                    JPanel temp = new JPanel() {
                        @Override
                        public void paintComponent(Graphics g) {
                            super.paintComponent(g);
                            g.setColor(Color.GREEN);
                            g.fillOval(boardCoord[finalI][finalJ][0] + pixelGap / 2 - moveIndicatorSize / 2, boardCoord[finalI][finalJ][1] + pixelGap / 2 - moveIndicatorSize / 2, moveIndicatorSize, moveIndicatorSize);
                            //g.drawOval(0, 0, 1000, 1000);
                        }
                    };
                    temp.setVisible(true);
                    //temp.setBounds(boardCoord[finalI][finalJ][0] - moveIndicatorSize / 2, boardCoord[finalI][finalJ][1] - moveIndicatorSize / 2, moveIndicatorSize, moveIndicatorSize);
                    temp.setOpaque(false);
                    jframe.add(temp);
                    toRemoveValidMovesPics.add(temp);
                    jframe.setComponentZOrder(temp, 0);
                    //temp.setVisible(false);
                }
                //panel1.removeAll();
                /*try {
                    Thread.sleep(100000);
                } catch (InterruptedException e) {

                }*/


    //panel1.revalidate();
    //panel1.repaint();

       /*         jframe.revalidate();
                jframe.repaint();
            }
        }
    }*/

    public void displayValidMovesPics() {
        for (int i = 0; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                if (validMovesOfPiece[i][j] == 1) {
                    JPanel temp = validMovesPics[i][j];
                    toRemoveValidMovesPics.add(temp);
                    temp.setVisible(true);
                    //jframe.add(temp);
                    jframe.setComponentZOrder(temp, 0);

                    jframe.revalidate();
                    jframe.repaint();
                }
            }
        }
        //panel1.revalidate();
        //panel1.repaint();


    }

    public void addInitialValidMoves() {
        for (int i = 0; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                int finalI = i;
                int finalJ = j;
                JPanel temp = new JPanel() {
                    @Override
                    public void paintComponent(Graphics g) {
                        super.paintComponent(g);
                        g.setColor(new Color(110, 110, 110, 90));
                        g.fillOval(boardCoord[finalI][finalJ][0] + pixelGap / 2 - moveIndicatorSize / 2, boardCoord[finalI][finalJ][1] + pixelGap / 2 - moveIndicatorSize / 2, moveIndicatorSize, moveIndicatorSize);
                        //g.drawOval(0, 0, 1000, 1000);
                    }
                };
                temp.setVisible(false);
                //temp.setBounds(boardCoord[finalI][finalJ][0] - moveIndicatorSize / 2, boardCoord[finalI][finalJ][1] - moveIndicatorSize / 2, moveIndicatorSize, moveIndicatorSize);
                temp.setOpaque(false);
                panel1.add(temp);
                panel1.setComponentZOrder(temp, 0);
                //temp.setVisible(false);
                validMovesPics[i][j] = temp;

                jframe.revalidate();
            }
            jframe.repaint();
            //panel1.removeAll();
                /*try {
                    Thread.sleep(100000);
                } catch (InterruptedException e) {

                }*/


        }

    }

    public void resetValidMoves() {
        for (int i = 0; i < 8; ++i) {
            Arrays.fill(validMovesOfPiece[i], 0);
        }
    }

    public void newGame(){

    }

    public JButton[][] getButtonStatus() {
        return buttonStatus;
    }

    public void updateBoardStatus(String recentCommand) {
        if(recentCommand.equals("")){
            return;
        }
        int[] commandCoord = sr.decipherMove(recentCommand);
        int xBoardBef = commandCoord[0];
        int yBoardBef = commandCoord[1];
        int xBoardAft = commandCoord[2];
        int yBoardAft = commandCoord[3];

        JLabel imageToMove = pieceImageList[commandCoord[1]][commandCoord[0]];
        JLabel imageAtDest = pieceImageList[commandCoord[3]][commandCoord[2]];
        int startXCoord = boardCoord[commandCoord[1]][commandCoord[0]][0];
        int startYCoord = boardCoord[commandCoord[1]][commandCoord[0]][1];
        int endXCoord = boardCoord[commandCoord[3]][commandCoord[2]][0];
        int endYCoord = boardCoord[commandCoord[3]][commandCoord[2]][1];
        int xCoordDiff = endXCoord - startXCoord;
        int yCoordDiff = endYCoord - startYCoord;

        int animationFrame = 5;
        //TODO: Implement later
        /*for(int i = 1; i <= animationFrame; ++i){
            imageToMove.setBounds(startXCoord + (xCoordDiff / animationFrame * i) + pieceImageConstantMod, startYCoord + (yCoordDiff / animationFrame * i) + pieceImageConstantMod, pieceImgSize, pieceImgSize);
            panel1.add(imageToMove);
            panel1.setComponentZOrder(imageToMove, 0);
            jframe.revalidate();
            jframe.repaint();
        }*/

        imageToMove.setBounds(endXCoord + pieceImageConstantMod, endYCoord + pieceImageConstantMod, pieceImgSize, pieceImgSize);
        panel1.add(imageToMove);
        panel1.setComponentZOrder(imageToMove, 0);
        //jframe.revalidate();
        //jframe.repaint();

        //Castle
        if(imageToMove.getName().equals("WKi")){
            JLabel wRook;
            if(xBoardAft - xBoardBef == 2){//short
                wRook = pieceImageList[0][7];
                wRook.setBounds(boardCoord[0][5][0] + pieceImageConstantMod, endYCoord + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                panel1.add(wRook);
                panel1.setComponentZOrder(wRook, 0);
                pieceImageList[0][7] = null;
                pieceImageList[0][5] = wRook;
            }
            else if(xBoardAft - xBoardBef == -2){
                wRook = pieceImageList[0][0];
                wRook.setBounds(boardCoord[0][3][0] + pieceImageConstantMod, endYCoord + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                panel1.add(wRook);
                panel1.setComponentZOrder(wRook, 0);
                pieceImageList[0][7] = null;
                pieceImageList[0][5] = wRook;
            }
        }
        else if(imageToMove.getName().equals("BKi")){
            JLabel bRook;
            if(xBoardAft - xBoardBef == 2){//short
                bRook = pieceImageList[7][7];
                bRook.setBounds(boardCoord[7][5][0] + pieceImageConstantMod, endYCoord + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                panel1.add(bRook);
                panel1.setComponentZOrder(bRook, 0);
                pieceImageList[7][7] = null;
                pieceImageList[7][5] = bRook;
            }
            else if(xBoardAft - xBoardBef == -2){
                bRook = pieceImageList[7][0];
                bRook.setBounds(boardCoord[7][3][0] + pieceImageConstantMod, endYCoord + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                panel1.add(bRook);
                panel1.setComponentZOrder(bRook, 0);
                pieceImageList[7][7] = null;
                pieceImageList[7][5] = bRook;
            }
        }
        else if(((imageToMove.getName().equals("WPa") || imageToMove.getName().equals("BPa")) && xBoardBef != xBoardAft && imageAtDest == null)){//ENPASSANTE
            JLabel pawnToRemove = pieceImageList[yBoardBef][xBoardAft];
            pawnToRemove.setVisible(false);
        }
        /*else if(imageToMove.getName().equals("BPa") && xBoardBef != xBoardAft && imageAtDest == null){
            JLabel pawnToRemove = pieceImageList[yBoardBef][xBoardAft];
            pawnToRemove.setVisible(false);
        }*/



        if(imageAtDest != null){
            imageAtDest.setVisible(false);
        }
        jframe.revalidate();
        jframe.repaint();
        pieceImageList[commandCoord[3]][commandCoord[2]] = imageToMove;
        pieceImageList[commandCoord[1]][commandCoord[0]] = null;
    }

    public void initialPicSetup() {
        String temp;
        for (int i = 0; i < 8; ++i) {
            for (int j = 0; j < 8; ++j) {
                if (boardStatus[i][j] != null) {
                    JLabel pieceImg = null;
                    temp = boardStatus[i][j].toString();
                        switch (temp) {
                        case "BKi":
                            pieceImg = new JLabel(new ImageIcon("images/Piece/BKingT.png"));
                            pieceImg.setBounds(boardCoord[i][j][0] + pieceImageConstantMod, boardCoord[i][j][1] + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                            break;
                        case "BRo":
                            pieceImg = new JLabel(new ImageIcon("images/Piece/BRookT.png"));
                            pieceImg.setBounds(boardCoord[i][j][0] + pieceImageConstantMod, boardCoord[i][j][1] + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                            break;
                        case "BKn":
                            pieceImg = new JLabel(new ImageIcon("images/Piece/BKnightT.png"));
                            pieceImg.setBounds(boardCoord[i][j][0] + pieceImageConstantMod, boardCoord[i][j][1] + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                            break;
                        case "BBi":
                            pieceImg = new JLabel(new ImageIcon("images/Piece/BBishopT.png"));
                            pieceImg.setBounds(boardCoord[i][j][0] + pieceImageConstantMod, boardCoord[i][j][1] + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                            break;
                        case "BQu":
                            pieceImg = new JLabel(new ImageIcon("images/Piece/BQueenT.png"));
                            pieceImg.setBounds(boardCoord[i][j][0] + pieceImageConstantMod, boardCoord[i][j][1] + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                            break;
                        case "BPa":
                            pieceImg = new JLabel(new ImageIcon("images/Piece/BPawnT.png"));
                            pieceImg.setBounds(boardCoord[i][j][0] + pieceImageConstantMod, boardCoord[i][j][1] + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                            break;
                        case "WKi":
                            pieceImg = new JLabel(new ImageIcon("images/Piece/WKingT.png"));
                            pieceImg.setBounds(boardCoord[i][j][0] + pieceImageConstantMod, boardCoord[i][j][1] + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                            break;
                        case "WRo":
                            pieceImg = new JLabel(new ImageIcon("images/Piece/WRookT.png"));
                            pieceImg.setBounds(boardCoord[i][j][0] + pieceImageConstantMod, boardCoord[i][j][1] + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                            break;
                        case "WKn":
                            pieceImg = new JLabel(new ImageIcon("images/Piece/WKnightT.png"));
                            pieceImg.setBounds(boardCoord[i][j][0] + pieceImageConstantMod, boardCoord[i][j][1] + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                            break;
                        case "WBi":
                            pieceImg = new JLabel(new ImageIcon("images/Piece/WBishopT.png"));
                            pieceImg.setBounds(boardCoord[i][j][0] + pieceImageConstantMod, boardCoord[i][j][1] + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                            break;
                        case "WQu":
                            pieceImg = new JLabel(new ImageIcon("images/Piece/WQueenT.png"));
                            pieceImg.setBounds(boardCoord[i][j][0] + pieceImageConstantMod, boardCoord[i][j][1] + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                            break;
                        case "WPa":
                            pieceImg = new JLabel(new ImageIcon("images/Piece/WPawnT.png"));
                            pieceImg.setBounds(boardCoord[i][j][0] + pieceImageConstantMod, boardCoord[i][j][1] + pieceImageConstantMod, pieceImgSize, pieceImgSize);
                            break;
                    }
                    if(pieceImg != null) {
                        pieceImg.setName(temp);
                        pieceImg.setVisible(true);
                        panel1.add(pieceImg);
                        panel1.setComponentZOrder(pieceImg, 0);
                        pieceImageList[i][j] = pieceImg;

                        jframe.revalidate();
                        jframe.repaint();
                    }

                }

            }
        }
    }

    public void addBasicInterfaceComponents(){
        //Timer
        jframe.revalidate();
        jframe.repaint();
        blackTimerLabel = new JLabel();
        blackTimerLabel.setVisible(true);
        blackTimerLabel.setBounds(jframe.getSize().width / 14 * 11, jframe.getSize().height / 4, 100, 50);
        panel1.add(blackTimerLabel);
        panel1.setComponentZOrder(blackTimerLabel, 0);


        whiteTimerLabel = new JLabel();
        whiteTimerLabel.setVisible(true);
        whiteTimerLabel.setBounds(jframe.getSize().width / 14 * 11, jframe.getSize().height / 4 * 3, 100, 50);
        panel1.add(whiteTimerLabel);
        panel1.setComponentZOrder(whiteTimerLabel, 0);

        //blackTimerLabel.setBounds(0, 0, 200, 100);
        //whiteTimerLabel.setBounds(0, 0, 200, 100);

        //blackTimerLabel.setLocation(100, 100);
        //whiteTimerLabel.setLocation(100, 200);

        invisibleLabel = new JLabel("");
        invisibleLabel.setVisible(true);
        panel1.add(invisibleLabel);
        panel1.setComponentZOrder(invisibleLabel, 0);

        //jframe.revalidate();
        //jframe.repaint();

        blackTimerLabel.setText(timeFormat(blackTimeLeft));
        whiteTimerLabel.setText(timeFormat(whiteTimeLeft));
        blackTimerLabel.setOpaque(true);
        whiteTimerLabel.setOpaque(true);

        blackTimerLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
        whiteTimerLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));

        //jframe.revalidate();
        //jframe.repaint();


        blackTimer = new Timer(100, new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if(blackTimeLeft <= 0){
                    System.out.println("WHITE WIN");//TODO: IMPLEMENT LATER
                }
                else if(blackTimeLeft >= 10 && blackTimeLeft % 1  <= 0.1){
                    blackTimerLabel.setText(timeFormat(blackTimeLeft));
                }
                else if(blackTimeLeft < 10){
                    blackTimerLabel.setText(Double.toString(blackTimeLeft));
                }
                long timeRightNow = System.currentTimeMillis();
                blackTimeLeft -= (double)(timeRightNow - timeInMillisWhenChange) / 1000;
                timeInMillisWhenChange = timeRightNow;
            }
        });

        whiteTimer = new Timer(100, new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if(whiteTimeLeft <= 0){
                    System.out.println("BLACK WIN");//TODO: IMPLEMENT LATER
                }
                else if(whiteTimeLeft >= 10 && whiteTimeLeft % 1  <= 0.1){
                    whiteTimerLabel.setText(timeFormat(whiteTimeLeft));
                }
                else if(whiteTimeLeft < 10){
                    whiteTimerLabel.setText(Double.toString(whiteTimeLeft));
                }
                //System.out.println(whiteTimeLeft);
                long timeRightNow = System.currentTimeMillis();
                whiteTimeLeft -= (double)(timeRightNow - timeInMillisWhenChange) / 1000;
                timeInMillisWhenChange = timeRightNow;
            }
        });


        //Username
        blackUserLabel = new JLabel(blackUserName);
        blackUserLabel.setBounds(jframe.getSize().width / 14 * 11, jframe.getSize().height / 4 - 50, 100, 50);
        blackUserLabel.setVisible(true);
        blackUserLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
        panel1.add(blackUserLabel);
        panel1.setComponentZOrder(blackUserLabel, 0);

        whiteUserLabel = new JLabel(whiteUserName);
        whiteUserLabel.setBounds(jframe.getSize().width / 14 * 11, jframe.getSize().height / 4 * 3 - 50, 100, 50);
        whiteUserLabel.setVisible(true);
        whiteUserLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
        panel1.add(whiteUserLabel);
        panel1.setComponentZOrder(whiteUserLabel, 0);

        panel1.revalidate();
        panel1.repaint();

        jframe.revalidate();
        jframe.repaint();
    }

    public void startSelectScreen(){
        jframe.setLayout(null);
        singleplayer = new JButton();
        multiplayer = new JButton();
        jframe.setSize(600, 400);
        singleplayer.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                singleplayerSetUp();
            }
        });

        multiplayer.setText("Multiplayer");
        singleplayer.setText("Singleplayer");
        singleplayer.setBounds(jframe.getSize().width / 2 - modeSelectButtonWidth / 2, jframe.getSize().height / 2, modeSelectButtonWidth, modeSelectButtonHeight);
        multiplayer.setBounds(jframe.getSize().width / 2 - modeSelectButtonWidth / 2, jframe.getSize().height / 16 * 11, modeSelectButtonWidth, modeSelectButtonHeight);
        singleplayer.setVisible(true);
        multiplayer.setVisible(true);

        multiplayer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                multiplayerSetUp();
            }
        });

        jframe.add(singleplayer);
        jframe.add(multiplayer);

        jframe.revalidate();
        jframe.repaint();
        jframe.setLayout(new BorderLayout());
    }

    public void singleplayerSetUp(){
        jframe.getContentPane().removeAll();
        jframe.revalidate();
        jframe.setSize(1200, 800);
        jframe.revalidate();
        jframe.repaint();

        panel1.setLayout(null);
        jframe.add(panel1);
        addButton();
        panel1.setVisible(true);
        addBasicInterfaceComponents();

        validMovesOfPiece = new int[8][8];
        boardStatus = board.getBoardStatus();
        piecePicked = null;
        try {
            board.start();
        } catch (BlackKingNotFoundException d) {

        } catch (WhiteKingNotFoundException d) {

        }
        addInitialValidMoves();
        initialPicSetup();

        //jframe.add(invisibleLabel);
        //jframe.revalidate();
        //jframe.repaint();
        //recentCommand = "";
    }

    public void multiplayerSetUp(){
        jframe.getContentPane().removeAll();
        jframe.setSize(1200, 800);
        jframe.revalidate();
        jframe.repaint();
        int jframeWidth = jframe.getSize().width;
        int jframeHeight = jframe.getSize().height;

        JLabel usernamePic = new JLabel("User Name");
        usernamePic.setVisible(true);
        usernamePic.setBounds(jframeWidth / 7 * 2, jframeHeight / 8, 100, 50);
        panel1.add(usernamePic);

        JLabel roomCodePic = new JLabel("Room Code");
        roomCodePic.setVisible(true);
        roomCodePic.setBounds(jframeWidth / 7 * 2, jframeHeight / 8, 100, 50);
        panel1.add(roomCodePic);

        JTextField usernameInput = new JTextField("Input Username");
        JTextField roomCodeInput = new JTextField("Create your own room code or submit a pre-created room code");
        roomCodeInput.setVisible(true);
        usernameInput.setVisible(true);
        roomCodeInput.setBounds(jframeWidth / 7 * 3, jframeHeight / 8, 200, 50);
        usernameInput.setBounds(jframeWidth / 7 * 3, jframeHeight / 8, 200, 50);

        panel1.add(roomCodeInput);
        panel1.add(usernameInput);

        panel1.revalidate();
        panel1.repaint();


    }

    private void whiteWin(){

    }

    private void blackWin(){

    }

    private String timeFormat(double timeInSecond){
        return (Integer.toString((int)timeInSecond / 60) + " : " + String.format("%02d", ((int)timeInSecond % 60)));
    }

    private void switchTimeAfterTurn(){
        if(board.getCurrTurn().equals("B")){
            whiteTimer.stop();
            blackTimer.start();
        }
        else{
            blackTimer.stop();
            whiteTimer.start();
        }
    }

    private void showTimerOnTop(){
        panel1.setComponentZOrder(blackTimerLabel, 0);
        panel1.setComponentZOrder(whiteTimerLabel, 0);
        panel1.setComponentZOrder(blackUserLabel, 0);
        panel1.setComponentZOrder(whiteUserLabel, 0);

        jframe.revalidate();
        jframe.repaint();
    }
}