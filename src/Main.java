import javax.swing.*;


public class Main {
    public static void main(String[] args) {
        Board board = new Board();
        ClientSide cs = new ClientSide();
        ChessGui cg = new ChessGui(board, cs);
    }
}
