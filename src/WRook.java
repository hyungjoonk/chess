public class WRook extends Piece {

    public WRook(int xCoord, int yCoord, Piece[][] boardStatus) {
        super(xCoord, yCoord, "W", boardStatus);
        giveWKingRef();
    }

    @Override
    public void calcValidMoves() {
        if (wking.isUnderCheck()) {
            calcUnderCheck();
            return;
        }
        possibleMoves = 0;
        resetValidMoves();
        Piece objAtDest;
        int tempXCoord = xCoord;
        int tempYCoord = yCoord;

        ++tempXCoord;
        if (indexInRange(tempXCoord, tempYCoord) && !moveLeaveKingCheck(tempXCoord, tempYCoord)) {
            for (; indexInRange(tempXCoord, tempYCoord); ++tempXCoord) {
                objAtDest = boardStatus[tempYCoord][tempXCoord];
                if (objAtDest != null) {
                    if (objAtDest.team.equals(this.team)) {
                        break;
                    }
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                    break;
                }
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        --tempXCoord;
        if (indexInRange(tempXCoord, tempYCoord) && !moveLeaveKingCheck(tempXCoord, tempYCoord)) {
            for (; indexInRange(tempXCoord, tempYCoord); --tempXCoord) {
                objAtDest = boardStatus[tempYCoord][tempXCoord];
                if (objAtDest != null) {
                    if (objAtDest.team.equals(this.team)) {
                        break;
                    }
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                    break;
                }
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        ++tempYCoord;
        if (indexInRange(tempXCoord, tempYCoord) && !moveLeaveKingCheck(tempXCoord, tempYCoord)) {
            for (; indexInRange(tempXCoord, tempYCoord); ++tempYCoord) {
                objAtDest = boardStatus[tempYCoord][tempXCoord];
                if (objAtDest != null) {
                    if (objAtDest.team.equals(this.team)) {
                        break;
                    }
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                    break;
                }
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        --tempYCoord;
        if (indexInRange(tempXCoord, tempYCoord) && !moveLeaveKingCheck(tempXCoord, tempYCoord)) {
            for (; indexInRange(tempXCoord, tempYCoord); --tempYCoord) {
                objAtDest = boardStatus[tempYCoord][tempXCoord];
                if (objAtDest != null) {
                    if (objAtDest.team.equals(this.team)) {
                        break;
                    }
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                    break;
                }
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }
    }

    private void calcUnderCheck() {
        possibleMoves = 0;
        resetValidMoves();
        Piece objAtDest;
        int tempXCoord = xCoord;
        int tempYCoord = yCoord;

        ++tempXCoord;
        for (; indexInRange(tempXCoord, tempYCoord); ++tempXCoord) {
            objAtDest = boardStatus[tempYCoord][tempXCoord];
            if (objAtDest != null) {
                if (objAtDest.team.equals(this.team)) {
                    break;
                }

                if(!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                }
                break;
            }
            if(!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }


        tempXCoord = xCoord;
        tempYCoord = yCoord;
        --tempXCoord;
        for (; indexInRange(tempXCoord, tempYCoord); --tempXCoord) {
            objAtDest = boardStatus[tempYCoord][tempXCoord];
            if (objAtDest != null) {
                if (objAtDest.team.equals(this.team)) {
                    break;
                }
                if(!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                }
                break;
            }
            if(!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }

        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        ++tempYCoord;

        for (; indexInRange(tempXCoord, tempYCoord); ++tempYCoord) {
            objAtDest = boardStatus[tempYCoord][tempXCoord];
            if (objAtDest != null) {
                if (objAtDest.team.equals(this.team)) {
                    break;
                }
                if(!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                }
                break;
            }
            if(!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }

        tempXCoord = xCoord;
        tempYCoord = yCoord;
        --tempYCoord;
        for (; indexInRange(tempXCoord, tempYCoord); --tempYCoord) {
            objAtDest = boardStatus[tempYCoord][tempXCoord];
            if (objAtDest != null) {
                if (objAtDest.team.equals(this.team)) {
                    break;
                }
                if(!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                    validMoves[tempYCoord][tempXCoord] = 1;
                    ++possibleMoves;
                }
                break;
            }
            if(!moveLeaveKingCheck(tempXCoord, tempYCoord)) {
                validMoves[tempYCoord][tempXCoord] = 1;
                ++possibleMoves;
            }
        }
    }

    public String toString() {
        return "WRo";
    }
}