import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class ChessGuiTest {

    private ChessGui cg;
    private Board board;
    private JButton[][] buttonStatus;
    private JFrame jframe;
    private JPanel panel1;
    private ClientSide cs;

    @BeforeEach
    void setUp() {
        cs = new ClientSide();
        board = new Board();
        cg = new ChessGui(board, cs);
        buttonStatus = cg.getButtonStatus();
        jframe = cg.getFrame();
        panel1 = cg.getMainPanel();
    }

    @Test
    void testAddValidMovesPic() {
        buttonStatus[1][4].doClick();

        Component[] components = jframe.getContentPane().getComponents();
        Component[] mainPanelComp = ((JPanel) components[0]).getComponents();

        int count = 1;
        /*for (Component component : mainPanelComp) {
            System.out.println(component.getClass().getName());
            System.out.println(count++);
        }

        for (Component component : components) {
            System.out.println(component.getClass().getName());
            System.out.println(count++);
        }*/

        jframe.pack();


        components = jframe.getContentPane().getComponents();
        mainPanelComp = ((JPanel) components[0]).getComponents();

        for (Component component : components) {
            System.out.println(component.getClass().getName());
            System.out.println(count++);
        }

        /*for (Component component : mainPanelComp) {
            System.out.println(component.getClass().getName());
            System.out.println(count++);
        }*/

        //jframe.






        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {

        }
    }

    @Test
    public void testAddingPicOne(){
        JPanel temp = new JPanel(){
            @Override
            public void paintComponent(Graphics g){
                super.paintComponent(g);
                g.setColor(Color.GREEN);
                g.fillOval(100, 100, 100, 100);
                //g.drawOval(0, 0, 1000, 1000);
            }
        };

        temp.setOpaque(false);
        panel1.add(temp);
        jframe.setComponentZOrder(temp, 0);

        temp = new JPanel(){
            @Override
            public void paintComponent(Graphics g){
                super.paintComponent(g);
                g.setColor(Color.RED);
                g.fillOval(200, 200, 100, 100);
                //g.drawOval(0, 0, 1000, 1000);
            }
        };

        temp.setOpaque(false);
        panel1.add(temp);
        jframe.setComponentZOrder(temp, 0);


        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {

        }

    }

    @Test
    void removeValidPics(){
        int[][] test = new int[8][8];



        jframe.removeAll();

        JPanel temp = new JPanel(){
            @Override
            public void paintComponent(Graphics g){
                super.paintComponent(g);
                g.setColor(Color.GREEN);
                g.fillOval(0, 0, 100, 100);
                //g.drawOval(0, 0, 1000, 1000);
            }
        };
        panel1 = new JPanel();
        jframe.add(panel1);
        jframe.setContentPane(panel1);
        temp.setVisible(true);
        //temp.setBounds(boardCoord[finalI][finalJ][0] - moveIndicatorSize / 2, boardCoord[finalI][finalJ][1] - moveIndicatorSize / 2, moveIndicatorSize, moveIndicatorSize);
        temp.setOpaque(false);
        panel1.add(temp);
        jframe.setComponentZOrder(temp, 0);
        //panel1.remove(temp);
        jframe.revalidate();
        jframe.repaint();



        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {

        }
    }
}